from flask import Flask
import psutil


app = Flask(__name__)


def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


@app.route('/')
def war():
    if checkIfProcessRunning('war3.exe'):
        return  'ESTOY JUGANDO!'
    else:
        return  'CORTA NOMAS!'

if __name__ == '__main__':
    app.run(host='0.0.0.0')

